import React from "react";
import { StyleSheet, View } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";
import MealList from "../components/MealList";
import { useSelector } from "react-redux";
import DefaultText from "../components/DelaultText";

const FavouriteScreen = (props) => {
  const favMeals = useSelector((state) => state.meals.favouriteMeals);
  // const favMeals = availableMeals.filter(
  //   (meal) => meal.id === "m1" || meal.id === "m2"
  // );

  if (favMeals.length === 0 || !favMeals) {
    return (
      <View style={styles.content}>
        <DefaultText>No favourite meals found. Start adding some!</DefaultText>
      </View>
    );
  }
  return <MealList listData={favMeals} navigation={props.navigation} />;
};

FavouriteScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Your Favourites!",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        ></Item>
      </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default FavouriteScreen;
